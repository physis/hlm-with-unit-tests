<?php

require "config.php";

header('Content-type: text/plain; charset=utf-8');

if (empty($_POST)) {
    echo "Invalid method";
    return;
}

// PDO does not show errors for multi-statement SQL so we execute one by one
// See https://bugs.php.net/bug.php?id=61613
function splitExec($db, $script)
{
    $sqls = explode(';', $script);
    foreach ($sqls as $sql) {
        $sql = trim($sql);
        if ($sql != '') {
            $db->exec(trim($sql));
        }
    }
}

$db = new PDO("mysql:host=".DB_HOST, DB_USER, DB_PWD);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

echo DB_USER . '@' . DB_HOST;

if (isset($_POST['create'])) {
    if (isset($_POST['db'])) {
        $db->exec("create database " . DB_DB);
        // NOTE: user `scripter' is not the same as the user used for connection (root or host-provided)
        $db->exec("create user scripter@" . DB_HOST . " identified by 'holama'");
        $db->exec("grant all on " . DB_DB . ".* to scripter@" . DB_HOST);
        $db->exec("flush privileges");
    }

    $db->exec("use ".DB_DB);

    splitExec($db, <<<EOT

        create table if not exists dword (
          dword_id integer not null auto_increment,
          word varchar(40) not null,
          primary key (dword_id)
        );

        create table if not exists dword_meaning (
          dword_meaning_id integer not null auto_increment,
          dword_id integer not null,
          meaning_txt varchar(250),
          primary key (dword_meaning_id),
          foreign key (dword_id) references dword(dword_id)
        );

        create table if not exists script (
          script_id integer not null auto_increment,
          title varchar(255) not null,
          is_deleted boolean not null default 0,
          status_id varchar(1) not null default 'I', -- I-nitial, L-ectoring
          source_script_id integer,
          primary key (script_id),
          foreign key (source_script_id) references script(script_id)
        );

        create table if not exists sentence (
          sentence_id integer not null auto_increment,
          script_id integer not null,
          meaning_txt varchar(10000),
          order_no integer not null,
          source_sentence_id integer,
          primary key (sentence_id),
          foreign key (script_id) references script(script_id),
          foreign key (source_sentence_id) references sentence(sentence_id)
        );

        create table if not exists sword (
          sword_id integer not null auto_increment,
          sentence_id integer not null,
          word varchar(40) not null,
          word_suffix varchar(40),
          dword_id integer,
          dword_meaning_id integer,
          grammar_form varchar(40),
          origin_txt varchar(250),
          stylistics_txt varchar(250),
          spelling_txt varchar(250),
          context_txt varchar(250),
          order_no integer not null,
          source_sword_id integer,
          primary key (sword_id),
          foreign key (sentence_id) references sentence(sentence_id),
          foreign key (dword_id) references dword(dword_id),
          foreign key (dword_meaning_id) references dword_meaning(dword_meaning_id),
          foreign key (source_sword_id) references sword(sword_id)
        );

        create table if not exists sentence_audio (
          sentence_audio_id integer not null auto_increment,
          sentence_id integer not null,
          locale varchar(8) not null, -- e.g. en_US, en_UK
          filename varchar(100) not null, -- just the filename, not the full path
          primary key (sentence_audio_id),
          foreign key (sentence_id) references sentence(sentence_id)
        );

        create table if not exists user (
          user_id integer not null auto_increment,
          user_name varchar(32) not null,
          password varchar(32) not null,
          primary key (user_id)
        );
EOT
);

    echo "DONE";
} elseif (isset($_POST['drop'])) {
    if (isset($_POST['db'])) {
        $db->exec("drop database if exists " . DB_DB);
        // "drop user if exists" workaround, see http://bugs.mysql.com/bug.php?id=19166
        // NOTE: user `scripter' is not the same as the root user used for connection  (root or host-provided)
        $db->exec("grant usage on *.* to scripter@" . DB_HOST);
        $db->exec("drop user scripter@" . DB_HOST);
        $db->exec("flush privileges");
    } else {
        $db->exec("use ".DB_DB);
        splitExec($db, <<<EOT
            drop table if exists user;
            drop table if exists sentence_audio;
            drop table if exists sword;
            drop table if exists sentence;
            drop table if exists script;
            drop table if exists dword_meaning;
            drop table if exists dword;
EOT
);
    }

    echo "DONE";
} else {
    echo "Missing  or invalid command";
}

$db = NULL;
