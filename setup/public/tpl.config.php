<?php

ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);

define('DB_HOST', '@DB_HOST@');
define('DB_DB', '@DB_DB@');
define('DB_USER', '@DB_USER@');
define('DB_PWD', '@DB_PWD@');

