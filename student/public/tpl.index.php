<?php

    define(ROOT_DIR, dirname(__FILE__));
    define(APP_DIR, ROOT_DIR . '/application');
    define(VIEW_DIR, APP_DIR . '/view'); // directory for views
    
    ini_set('display_errors', 1);
    error_reporting(E_ALL | E_STRICT);

    set_include_path(str_replace('{public}', ROOT_DIR, '@INCLUDE_PATH_PREPEND@') . PATH_SEPARATOR . get_include_path());

    // set up class autoloader, see spl_autoload_register
    function hierAutoload($className)
    {
        $path = str_replace('_', '/', $className);
        // e.g. class Foo_Bar -> Foo/Bar.php
        require "$path.php";
    }
    spl_autoload_register('hierAutoload');

    $views = array(
    );

    // load and activate MVC framework
    $mvc = new Mvc($views);
    $mvc->dispatch();
