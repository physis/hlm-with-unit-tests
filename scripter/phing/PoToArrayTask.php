<?php

class PoToArrayTask extends Task
{

    private $pofile;
    public function setPofile($pofile)
    {
        $this->pofile = $pofile;
    }
    
    private $phpfile;
    public function setPhpfile($phpfile)
    {
        $this->phpfile = $phpfile;
    }
    
    public function main()
    {
        ob_start();
        echo "<?php\n";
        $this->echoArray();
        $arrayStr = ob_get_clean();
        file_put_contents($this->phpfile, $arrayStr);
    }
    
    private function echoArray()
    {
        $msgid = NULL;
        $lines = file($this->pofile);
        foreach ($lines as $line) {
            preg_match('/^msgid "(.*)"/', $line, $matches);
            if (isset($matches[1])) {
                $msgid = $this->esc($matches[1]);
                continue;
            }
            preg_match('/^msgstr "(.*)"/', $line, $matches);
            if (isset($matches[1])) {
                $msgstr = $this->esc($matches[1]);
                echo "\$message['$msgid'] = '$msgstr';\n";
                continue;
            }
        }
    }
    
    private function esc($str)
    {
        return str_replace("'", "\\'", $str);
    }
}
