<?php

class Db
{

    private static $db;

    private static function createDb()
    {
        $db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_DB, DB_USER, DB_PWD);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    }

    public static function getDb()
    {
        if (!isset(Db::$db)) {
            Db::$db = Db::createDb();
        }
        return Db::$db;
    }
}
