<?php

class Dao_DWordDao
{

    public static function mapToDWord($map)
    {
        $id = $map['dword_id'];
        $word = $map['word'];
        $dword = new Domain_DWord($id, $word);
        return $dword;
    }

    public static function mapToDWordMeaning($map)
    {
        return new Domain_DWordMeaning(
                $map['dword_meaning_id'], $map['dword_id'], $map['meaning_txt']);
    }

    public function getIdOrNullByWord($word)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare("select dword_id from dword"
                    . " where word = :word");
            $st->bindParam(':word', $word);
            $st->execute();
            $rec = $st->fetch(PDO::FETCH_NUM);
        }
        $db = NULL;
        if ($rec) {
            return $rec[0];
        } else {
            return NULL;
        }
    }

    public function getDWordMeaningOrNullByDWordIdAndMeaningTxt($dwordId, $meaningTxt)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare('select * from dword_meaning'
                    . ' where dword_id = :dword_id'
                    . '   and meaning_txt = :meaning_txt');
            $st->bindParam(':dword_id', $dwordId);
            $st->bindParam(':meaning_txt', $meaningTxt);
            $st->execute();
            $rec = $st->fetch(PDO::FETCH_ASSOC);
            
        }
        $db = NULL;
        if ($rec) {
            return Dao_DWordDao::mapToDWordMeaning($rec);
        } else {
            return NULL;
        }
    }

    public function findMeaningsByDWord($dword)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare('select meaning_txt'
                    . ' from dword dw join dword_meaning dwm'
                    . '   on dw.dword_id = dwm.dword_id'
                    . ' where word = ?');
            $st->execute(array($dword));
            $meanings = $st->fetchAll(PDO::FETCH_COLUMN, 0);
        }
        $db = NULL;
        return $meanings;
    }

    public function findMapSwordIdToMeaningsBySentence($sentenceId)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare("select sw.sword_id, dwm.dword_meaning_id, dwm.meaning_txt"
                    . " from sword sw join dword dw"
                    . "   on sw.dword_id = dw.dword_id"
                    . " join dword_meaning dwm"
                    . "   on sw.dword_id = dwm.dword_id"
                    . " where sentence_id = :sentence_id"
                    . " order by sw.sword_id");
            $st->bindParam(':sentence_id', $sentenceId);
            $st->execute();
            $mapSwordIdToMeaningRecs = $st->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP); // map: swordId => array(dwordMeaningId, meaningTxt)
        }
        $db = NULL;
        
        $mapSwordIdToMeanings = array();
        foreach ($mapSwordIdToMeaningRecs as $swordId => $meaningRecs) {
            $dwordMeanings = array();
            foreach ($meaningRecs as $meaningRec) {
                $dwordMeanings[] = new Domain_DWordMeaning(
                        $meaningRec['dword_meaning_id'], $swordId, $meaningRec['meaning_txt']);
            }
            $mapSwordIdToMeanings[$swordId] = $dwordMeanings;
        }
        return $mapSwordIdToMeanings;
    }

    public function insertWord($wordTxt)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare('insert into dword (word) values (:word)');
            $st->bindParam(':word', $wordTxt);
            $st->execute();
            $dwordId = $db->lastInsertId();
        }
        $db = NULL;
        return $dwordId;
    }

    public function insertDWordMeaning($dwordMeaning)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare('insert into dword_meaning'
                    . ' (dword_meaning_id, dword_id, meaning_txt)'
                    . ' values (:dword_meaning_id, :dword_id, :meaning_txt)');
            $st->bindParam(':dword_meaning_id', $dwordMeaning->id);
            $st->bindParam(':dword_id', $dwordMeaning->dwordId);
            $st->bindParam(':meaning_txt', $dwordMeaning->meaningTxt);
            $st->execute();
            $dwordMeaningId = $db->lastInsertId();
        }
        $db = NULL;
        return $dwordMeaningId;
    }
}
