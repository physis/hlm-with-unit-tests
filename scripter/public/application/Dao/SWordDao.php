<?php

class Dao_SWordDao
{

    protected function mapToObject($map)
    {
        $sword = new Domain_SWord();
        $sword->id = $map['sword_id'];
        $sword->sentenceId = $map['sentence_id'];
        $sword->word = $map['word'];
        $sword->wordSuffix = $map['word_suffix'];
        $sword->dwordId = $map['dword_id'];
        $sword->dwordMeaningId = $map['dword_meaning_id'];
        $sword->grammarForm = $map['grammar_form'];
        $sword->originTxt = $map['origin_txt'];
        $sword->stylisticsTxt = $map['stylistics_txt'];
        $sword->spellingTxt = $map['spelling_txt'];
        $sword->contextTxt = $map['context_txt'];
        $sword->orderNo = $map['order_no'];
        $sword->sourceId = $map['source_sword_id'];
        return $sword;
    }

    public function findBySentenceId($sentenceId)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare('select sw.*, dw.word dict_word'
                    . ' from sword sw left join dword dw'
                    . '   on sw.dword_id = dw.dword_id'
                    . ' where sentence_id = :sentence_id'
                    . ' order by order_no');
            $st->bindParam(':sentence_id', $sentenceId);
            $st->execute();
            $rows = $st->fetchAll(PDO::FETCH_ASSOC);
            $swords = array();
            foreach ($rows as $row) {
                $sword = $this->mapToObject($row);
                $sword->dword = new Domain_DWord($row['dword_id'], $row['dict_word']);
                $swords[] =  $sword;
            }
            
        }
        $db = NULL;
        
        return $swords;
    }

    public function cloneByScriptId($newScriptId)
    {
        $db = Db::getDb();
        {
            // we join: source sword (ssw) <=> source sentence (sse) <=> new sentence (nse)
            $st = $db->prepare(<<<EOT
INSERT INTO sword (sentence_id, word, word_suffix, dword_id, dword_meaning_id, grammar_form, origin_txt, stylistics_txt, spelling_txt, context_txt, order_no, source_sword_id)
   SELECT nse.sentence_id, word, word_suffix, dword_id, dword_meaning_id, grammar_form, origin_txt, stylistics_txt, spelling_txt, context_txt, ssw.order_no, sword_id
   FROM sword ssw JOIN sentence sse
       ON ssw.sentence_id = sse.sentence_id
     JOIN sentence nse
       ON sse.sentence_id = nse.source_sentence_id
   WHERE nse.script_id = :new_script_id
EOT
                    );
            $st->bindParam(':new_script_id', $newScriptId);
            $st->execute();
        }
        $db = NULL;
    }
}