<?php

class Dao_UserDao
{
    public function findUserByName($name)
    {
        $db = Db::getDb();
        {
            $select_user = $db->prepare('SELECT * FROM user WHERE user_name = :user_name');
            $select_user->bindParam(':user_name', $name);
            $select_user->execute();
            $userMap = $select_user->fetch(PDO::FETCH_ASSOC);
        }
        $db = NULL;
        
        if ($userMap) {
            $user = new Domain_User();
            $user->id = $userMap['user_id'];
            $user->name = $userMap['user_name'];
            $user->password = $userMap['password'];
            return $user;
        } else {
            return FALSE;
        }
    }
}