<?php

class Dao_SentenceDao
{

    protected function mapToObject($map)
    {
        $sentence = new Domain_Sentence();
        $sentence->id = $map['sentence_id'];
        $sentence->scriptId = $map['script_id'];
        $sentence->meaningTxt = $map['meaning_txt'];
        $sentence->orderNo = $map['order_no'];
        $sentence->sourceId = $map['source_sentence_id'];
        return $sentence;
    }

    public function getById($sentenceId)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare('SELECT * FROM sentence WHERE sentence_id = :sentence_id');
            $st->bindParam(':sentence_id', $sentenceId);
            $st->execute();
            $row = $st->fetch(PDO::FETCH_ASSOC);
        }
        $db = NULL;

        if ($row) {
            return $this->mapToObject($row);
        } else {
            return FALSE;
        }
    }

    public function delete($sentenceId, $cascade = FALSE)
    {
        $db = Db::getDb();
        {
            if ($cascade) {
                $st = $db->prepare('delete from sword where sentence_id = ?');
                $st->execute(array($sentenceId));
            }
            $st = $db->prepare('delete from sentence where sentence_id = ?');
            $st->execute(array($sentenceId));
        }
        $db = NULL;
    }

    public function cloneByScriptId($newScriptId)
    {
        $db = Db::getDb();
        {
            // we join: source sentence (sse) <=> source script (ssc) <=> new script (nsc)
            $st = $db->prepare("INSERT INTO sentence (script_id, meaning_txt, order_no, source_sentence_id)"
                    . " SELECT nsc.script_id, meaning_txt, order_no, sentence_id"
                    . " FROM sentence sse JOIN script ssc"
                    . "     ON sse.script_id = ssc.script_id"
                    . "   JOIN script nsc"
                    . "     ON ssc.script_id = nsc.source_script_id"
                    . " WHERE nsc.script_id = :new_script_id");
            $st->bindParam(':new_script_id', $newScriptId);
            $st->execute();
        }
        $db = NULL;
    }
}