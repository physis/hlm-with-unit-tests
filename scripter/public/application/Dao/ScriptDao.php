<?php

class Dao_ScriptDao
{

    protected function mapToObject($map)
    {
        $script = new Domain_Script();
        $script->id = $map['script_id'];
        $script->title = $map['title'];
        $script->isDeleted = $map['is_deleted'];
        $script->statusId = $map['status_id'];
        $script->sourceId = $map['source_script_id'];
        return $script;
    }

    public function getById($scriptId)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare('SELECT * FROM script WHERE script_id = :script_id');
            $st->bindParam(':script_id', $scriptId);
            $st->execute();
            $row = $st->fetch(PDO::FETCH_ASSOC);
        }
        $db = NULL;
    
        if ($row) {
            return $this->mapToObject($row);
        } else {
            return FALSE;
        }
    }

    public function findAllActive()
    {
        $db = Db::getDb();
        {
            $rows = $db->query("select * from script where is_deleted = 0");
            $ret = array();
            foreach ($rows as $row) {
                $ret[] = $this->mapToObject($row);
            }
        }
        $db = NULL;
        return $ret;
    }

    public function insertTitle($title)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare("INSERT INTO script (title) VALUE (:title)");
            $st->bindParam(':title', $title);
            $st->execute();
        
            $scriptId = $db->lastInsertId('script_id');
        }
        $db = NULL;
        return $scriptId;
    }

    public function updateTitle($scriptId, $title)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare("update script"
                    . " set title = ?"
                    . " where script_id = ?");
            $success = $st->execute(array($title, $scriptId));
            $ret = $success ? $st->rowCount() : FALSE;
        }
        $db = NULL;
        return $ret;
    }

    public function markAsDeleted($scriptId)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare("UPDATE script SET is_deleted = 1 where script_id = ?");
            $success = $st->execute(array($scriptId));
            $ret = $success ? $st->rowCount() : FALSE;
        }
        $db = NULL;
        return $ret;
    }

    public function cloneWithStatus($scriptId, $statusId)
    {
        $db = Db::getDb();
        {
            $st = $db->prepare("INSERT INTO script (title, status_id, source_script_id)"
                    . " SELECT title, :status_id, :script_id FROM script"
                    . " WHERE script_id = :script_id");
            $st->bindParam(':script_id', $scriptId);
            $st->bindParam(':status_id', $statusId);
            $st->execute();
            $newScriptId = $db->lastInsertId('script_id');
        }
        $db = NULL;
        return $newScriptId;
    }

}