<?php

class Controller_Dict
{
    public function action()
    {
        header('Content-type: application/json');
        
        $dword = $_GET['word'];
        if (!isset($dword)) {
            header("HTTP/1.0 400 Bad Request");
            return NULL;
        }
        
        $dwordDao = new Dao_DWordDao();
        $meanings = $dwordDao->findMeaningsByDWord($dword);
        
        echo json_encode($meanings);
        
        return NULL; // we are done; no model and view needed
    }
}
