<?php
class Controller_Audioedit extends Controller_AbstractSubmittable
{

    protected function actionShow()
    {
        $sentence_id = $_GET['sentence_id'];
        $map = audioMap($sentence_id);
        $model = array(
            'templateTitle' => V::_('Audio file upload'),
            'sentence_id' => $sentence_id,
            'map' => $map,
            'errors' => array(),
        );
        return array('audioedit', $model);
    }

    protected function actionSubmit()
    {
        $sentence_id = $_POST['sentence_id'];  // to name file indexed by the sentence id
        $errors = array();

        $db = Db::getDb();
            $insert_audio = $db->prepare('INSERT INTO sentence_audio (sentence_id, locale, filename) VALUES (:sentence_id, :locale, :filename)');
            foreach ($_FILES as $locale => $filedata) {
                list($valid, $msg) = validate($filedata);
                if ($valid) {
                    $originalFullName = $filedata['name'];
                    $extension = extension($originalFullName);
                    $tmp = $filedata['tmp_name'];
                    $indexedName = $locale . '__ID' . $sentence_id;
                    $indexedFullName = $indexedName . '.' . $extension;
                    $dest = AUDIO_DIR . '/' . $indexedFullName;
                    move_uploaded_file($tmp, $dest);

                    $insert_audio->bindParam(':sentence_id', $sentence_id);
                    $insert_audio->bindParam(':locale', $locale);
                    $insert_audio->bindParam(':filename', $indexedFullName);
                    $insert_audio->execute();
                } else {
                    $errors[$locale] = $msg;
                }
            }

            $delete_audio = $db->prepare('DELETE FROM sentence_audio WHERE sentence_id = :sentence_id');
            foreach ($_POST as $deleterCode => $checked) {
                $parts = explode('__', $deleterCode);
                $validDelete = count($parts) == 2;
                if ($validDelete) {
                    list($locale, $extension) = $parts;
                    $indexedName = $locale . '__ID' . $sentence_id;
                    if ($extension) {
                        $indexedFullName = $indexedName . '.' . $extension;
                    } else {
                        $indexedFullName = $indexedName;
                    }
                    $dest = AUDIO_DIR . '/' . $indexedFullName;
                    unlink($dest);

                    $delete_audio->bindParam(':sentence_id', $sentence_id);
                    $delete_audio->execute();
                }
            }
        $db = NULL;

        $allValid = empty($errors);
        if ($allValid) {
            $script_id = hostScriptOf($sentence_id); // Mine out `host' $script_id out of the DB...
            // ... to know where to redirect back after successful audio upload
            if ($script_id == NULL) {
                throw new Exception(V::_("This sentence has no `host' script!"));
            } else {
                $backlink = "scriptedit?script_id=$script_id";
                $redirect = array("redirect:$backlink", NULL);
                return $redirect;
            }

        } else {
            $map = audioMap($sentence_id);
            $model = array(
                'templateTitle' => V::_('Audio file upload'),
                'sentence_id' => $sentence_id,
                'map' => $map,
                'errors' => $errors,
            );
            $reload = array('audioedit', $model);
            return $reload;
        }
    }

}

function validate($filedata)
{
    $sysError = $filedata['error'];
    $name = $filedata['name'];
    $extension = extension($name);
    if ($sysError) {
        $valid = FALSE;
        if (empty($name)) {
            $msg = V::_('You must select a file');
        } else {
            $msg = V::_("Upload error for file `$name'");
        }
    } elseif ($extension != 'mp3' && $extension != 'wav' && $extension != 'ogg') {
        $valid = FALSE;
        if (empty($extension)) {
            $msg = V::_('Filenames without any extension are not allowed');
        } else {
            $msg = V::_F("Extension `%s' is not allowed, thus file `%s' is not managed", array($extension, $name));
        }
    } else {
        $valid = TRUE;
        $msg = V::_('O. K.');
    }
    return array($valid, $msg);
}

function audioMap($sentence_id)
{
    $map = array();
    $db = Db::getDb();
        $select_audio = $db->prepare('SELECT * FROM sentence_audio WHERE sentence_id = :sentence_id');
        $select_audio->bindParam(':sentence_id', $sentence_id);
        $select_audio->execute();
        while ($audio_rec = $select_audio->fetch(PDO::FETCH_ASSOC)) {
            $locale = $audio_rec['locale'];
            $filename = $audio_rec['filename'];
            $url = AUDIO_DIR_HREF_BASE . "/$filename";
            $map[$locale] = $url;
        }
    $db = NULL;
    return $map;
}
?>
