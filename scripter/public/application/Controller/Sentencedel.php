<?php
class Controller_Sentencedel extends Controller_AbstractSubmittable
{

    protected function actionShow()
    {
        $sentenceId = $_GET['sentence_id'];

        $swordDao = new Dao_SWordDao();
        $swords = $swordDao->findBySentenceId($sentenceId);
        $sentenceTxt = "";
        foreach ($swords as $sword) {
            $sentenceTxt .= $sword->word . $sword->wordSuffix;
        }

        $model = array(
                'templateTitle' => V::_('Delete sentence'),
                'sentenceId' => $sentenceId,
                'sentenceTxt' =>$sentenceTxt,
                'confirm' => FALSE
        );

        return array('sentencedel', $model);
    }

    protected function actionSubmit()
    {
        $sentenceId = $_GET['sentence_id'];
        Check::checkIsset($sentenceId);

        $sentenceDao = new Dao_SentenceDao();
        $scriptId = $sentenceDao->getById($sentenceId)->scriptId;
        $sentenceDao->delete($sentenceId, TRUE);

        return array("redirect:scriptedit?script_id=$scriptId", NULL);
    }
}
