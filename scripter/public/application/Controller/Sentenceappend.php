<?php
class Controller_Sentenceappend extends Controller_AbstractSubmittable
{

    protected function actionShow()
    {
        $afterSentenceId = $_GET['after_sentence_id'];
        $model = array(
                'templateTitle' => V::_('Append sentence'),
                'afterSentenceId' => $afterSentenceId
        );
        return array('sentenceappend', $model);
    }

    protected function actionSubmit()
    {
        $afterSentenceId = $_POST['after_sentence_id'];
        $sentenceTxt = $_POST['sentence_txt'];
        Check::checkIsset($afterSentenceId);
        Check::checkIsset($sentenceTxt);

        $sentenceDao = new Dao_SentenceDao();
        $afterSentence = $sentenceDao->getById($afterSentenceId);
        if ($afterSentence === FALSE) throw new Exception("Sentence id not found: $afterSentenceId");

        $scriptId = $afterSentence->scriptId;
        $afterOrderNo = $afterSentence->orderNo;

        $sentenceParts = sentenceToBodyAndPunctuation($sentenceTxt);

        insert_sentence($sentenceParts[0], $sentenceParts[1], $scriptId, $afterOrderNo + 1, TRUE);

        return array("redirect:scriptedit?script_id=" . $scriptId, NULL);
    }

}
?>
