<?php
class Controller_Sentenceedit extends Controller_AbstractSubmittable
{

    protected function actionShow()
    {
        $sentenceId = $_GET['sentence_id'];

        $scriptDao = new Dao_ScriptDao();
        $swordDao = new Dao_SWordDao();
        $sentenceDao = new Dao_SentenceDao();
        $dwordDao = new Dao_DWordDao();
        // Word level
        $swords = $swordDao->findBySentenceId($sentenceId);
        
        // Sentence level
        $sentence = $sentenceDao->getById($sentenceId);
        $meaning = $sentence->meaningTxt;

        // Meaning level (multiple meanings for one word)
        $mapSwordIdToMeanings = $dwordDao->findMapSwordIdToMeaningsBySentence($sentenceId);
        foreach ($swords as &$sword) {
            $swordId = $sword->id;
            $sword->meanings = isset($mapSwordIdToMeanings[$swordId]) ? $mapSwordIdToMeanings[$swordId] : array();
        }

        // Source data (for lecturing)
        $script = $scriptDao->getById($sentence->scriptId);
        $sourceScriptId = $script->sourceId;
        $isLect = isset($sourceScriptId) && $sourceScriptId !== "";
        if ($isLect) {
            $sourceSentence = $sentenceDao->getById($sentence->sourceId);
            $sourceSwords = $swordDao->findBySentenceId($sourceSentence->id);
            $sourceSwordsMap = Controller_Sentenceedit::list_to_map($sourceSwords, 'id');
            $source = array('swordsMap' => $sourceSwordsMap, 'meaning' => $sourceSentence->meaningTxt);
        } else {
            $source = NULL;
        }


        $model = array(
            'templateTitle' => V::_('Edit sentence'),
            'sentence_id' => $sentenceId,
            'meaning' => $meaning,
            'swords' => $swords,
            'source' => $source
        );
        return array('sentenceedit', $model);
    }

    private static function list_to_map($list, $keyName)
    {
        $map = array();
        foreach ($list as $item) {
            $map[$item->$keyName] = $item;
        }
        return $map;
    }

    protected function actionSubmit()
    {
        $sentenceId = $_POST['sentence_id'];

        $dwordDao = new Dao_DWordDao();
        
        $db = Db::getDb();
        
            // Executing updates to form fields
            for ($i = 0; isset($_POST['swords'][$i]['sword_id']); ++$i) {
                $sword = $_POST['swords'][$i];

                $dictWord = $sword['dwordTxt'];
                if ($dictWord !== '') {
                    $dwordId = $dwordDao->getIdOrNullByWord($dictWord);
                    if ($dwordId === NULL) { // no such word yet in dict
                        $dwordId = $dwordDao->insertWord($dictWord);
                    }
                    
                    $actMeaning = $sword['act_meaning'];
                    if ($actMeaning !== '') {
                        $dwordMeaning = $dwordDao->getDWordMeaningOrNullByDWordIdAndMeaningTxt($dwordId, $actMeaning);
                        if ($dwordMeaning === NULL) { // no such meaning yet in dict
                            $dwordMeaningId = $dwordDao->insertDWordMeaning(
                                    new Domain_DWordMeaning(NULL, $dwordId, $actMeaning));
                        } else {
                            $dwordMeaningId = $dwordMeaning->id;
                        }
                    } else { // $actMeaning empty
                        $dwordMeaningId = NULL;
                    }
                } else { // empty dict word given
                    $dwordId = NULL;
                    $dwordMeaningId = NULL;
                }

                $st = $db->prepare("update sword set"
                        . "   dword_id = :dword_id,"
                        . "   grammar_form = :grammar_form,"
                        . "   dword_meaning_id = :dword_meaning_id,"
                        . "   origin_txt = :origin_txt,"
                        . "   stylistics_txt = :stylistics_txt,"
                        . "   spelling_txt = :spelling_txt,"
                        . "   context_txt = :context_txt"
                        . " where sword_id = :sword_id");
                $st->bindParam(':dword_id', $dwordId);
                $st->bindParam(':dword_meaning_id', $dwordMeaningId);
                $st->bindParam(':grammar_form', $sword['grammarForm']);
                $st->bindParam(':origin_txt', $sword['originTxt']);
                $st->bindParam(':stylistics_txt', $sword['stylisticsTxt']);
                $st->bindParam(':spelling_txt', $sword['spellingTxt']);
                $st->bindParam(':context_txt', $sword['contextTxt']);
                $st->bindParam(':sword_id', $sword['sword_id']);
                $st->execute();
                
            }

            // Executing updates to the sentence-level form-field(s) (`meaning_txt', for now)
            $st = $db->prepare("UPDATE sentence SET meaning_txt = :value where sentence_id = :sentence_id");
            $st->bindParam(':value', $_POST['meaning_txt']);
            $st->bindParam(':sentence_id', $sentenceId);
            $st->execute();

            // Detecting `host' script of the actual sentence and setting redirect accordingly:
            $script_id = hostScriptOf($sentenceId);

        $db = NULL;

        if ($script_id != NULL) {
            $redirect = array("redirect:scriptedit?script_id=$script_id", NULL);
        } else {
            throw new Exception(V::_("The sentence has no `host' script!"));
        }

        return $redirect;
    }

}
?>
