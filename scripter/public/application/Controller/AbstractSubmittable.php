<?php


abstract class Controller_AbstractSubmittable
{
    public final function action()
    {
        if ($this->isSubmission()) {
            return $this->actionSubmit();
        } else {
            return $this->actionShow();
        }
    }
    
    protected function isSubmission()
    {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }
    
    protected abstract function actionShow();

    protected abstract function actionSubmit();
    
}

