<?php
class Controller_Scriptnew extends Controller_AbstractSubmittable
{

    protected function actionShow()
    {
        $model = array(
            'templateTitle' => V::_('New script'),
            'scriptId' => "",
            'title' => "",
            'registeredSentences' => array(),
            'errors' => array(),
        );
        return array('scriptedit', $model);
    }

    protected function actionSubmit()
    {
        $title = $_POST['title'];
        $story = $_POST['story'];

        $errors = array();
        if (empty($title)) {
            $errors['title'] = V::_('Title must not be empty');
        };
        if (empty($story)) {
            $errors['story'] = V::_('Story must not be empty');
        }
        $holdingMode = !empty($errors);

        if ($holdingMode) {
            $model = array(
                'templateTitle' => V::_('New script'),
                'scriptId' => '',
                'title' => $title,
                'story' => $story,
                'errors' => $errors,
            );
            return array('scriptedit', $model);
        } else {
            $scriptDao = new Dao_ScriptDao();
            $script_id = $scriptDao->insertTitle($title);
            importStory($story, $script_id);
            return array('redirect:scriptlist', NULL);
        }
    }
}

?>
