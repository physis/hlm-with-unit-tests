<?php
class Controller_Scriptedit extends Controller_AbstractSubmittable
{

    protected function actionShow()
    {
        $script_id = $_GET['script_id'];

        // `exportStory...' aux function leverages that Db class maintains the same reference for each getting inside a http-request
        $registeredScript = exportStory_tripleArticulation($script_id);

        $title = $registeredScript['title'];
        $registeredSentences = $registeredScript['registeredSentences'];

        $model = array(
            'templateTitle' => V::_('Edit script'),
            'scriptId' => $script_id,
            'title' => $title,
            'registeredSentences' => $registeredSentences,
            'errors' => array(),
        );

        return array('scriptedit', $model);
    }

    protected function actionSubmit()
    {
        $script_id = $_POST['script_id'];
        $registeredScript = exportStory_tripleArticulation($script_id);

        $errors = array();

        $title = $_POST['title'];
        if (empty($title)) {
            $errors['title'] = V::_('Title must not be empty');
            $title = $registeredScript['title'];
        }
        $storyMode = isset($_POST['story']);
        if ($storyMode) {
            $story = $_POST['story'];
            if (empty($story)) {
                $errors['story'] = V::_('Story must not be empty');
            }
        }

        $holdingMode = !empty($errors);

        if ($holdingMode) {
            $model = array(
                'templateTitle' => V::_('Edit script'),
                'scriptId' => $script_id,
                'title' => $title,
                'errors' => $errors,
            );
            if ($storyMode) {
                $model['story'] = $story;
            } else {
                $registeredSentences = $registeredScript['registeredSentences'];
                $model['registeredSentences'] = $registeredSentences;
            }
            return array('scriptedit', $model);
        } else {
            $scriptDao = new Dao_ScriptDao();
            $scriptDao->updateTitle($script_id, $title);
            if ($storyMode) {
                importStory($story, $script_id); 
            }
            return array('redirect:scriptlist', NULL);
        }
    }
    
}

?>
