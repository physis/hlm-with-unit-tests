<?php

class Controller_Scriptlist
{
    public function action()
    {
        $model = array('templateTitle' => V::_('Script list'));

        $scriptDao = new Dao_ScriptDao();
        $rows = $scriptDao->findAllActive();

        $filteredRows = $this->filterOverridden($rows);

        $model['rows'] = $filteredRows;

        return array('scriptlist', $model);
    }

    private function filterOverridden($rows)
    {
        $shadowed = array(); // scripts that are not editable because e.g. they already have lectored copies
        foreach ($rows as $row) {
            if ($row->statusId === 'L' && isset($row->sourceId)) {
                $shadowed[$row->sourceId] = TRUE;
            }
        }
        
        $filteredRows = array();
        foreach ($rows as $row) {
            if (!isset($shadowed[$row->id])) $filteredRows[] = $row;
        }
        return $filteredRows;
    }
}
