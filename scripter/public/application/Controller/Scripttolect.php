<?php
class Controller_Scripttolect extends Controller_AbstractSubmittable
{

    protected function actionShow()
    {
        $scriptId = $_GET['script_id'];
        $model = array(
                'templateTitle' => V::_('Promote script to lector'),
                'scriptId' => $scriptId
        );
        return array('scripttolect', $model);
    }

    protected function actionSubmit()
    {
        $scriptId = $_POST['script_id'];
        Check::checkIsset($scriptId);

        $scriptDao = new Dao_ScriptDao();
        $newScriptId = $scriptDao->cloneWithStatus($scriptId, 'L');

        $sentenceDao = new Dao_SentenceDao();
        $sentenceDao->cloneByScriptId($newScriptId);

        $swordDao = new Dao_SWordDao();
        $swordDao->cloneByScriptId($newScriptId);

        return array("redirect:scriptlist", NULL);
    }

}
?>
