<?php
class Controller_Scriptdel extends Controller_AbstractSubmittable
{

    protected function actionShow()
    {
        $scriptId = $_GET['script_id'];

        $model = array(
                'templateTitle' => V::_('Delete script'),
                'scriptId' => $scriptId,
                'confirm' => FALSE
        );
    
        return array('scriptdel', $model);
    }

    protected function actionSubmit()
    {
        $scriptId = $_GET['script_id'];
        
        $scriptDao = new Dao_ScriptDao();
        $scriptDao->markAsDeleted($scriptId);

        return array('redirect:scriptlist', NULL);
    }
}
