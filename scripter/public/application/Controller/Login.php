<?php

class Controller_Login extends Controller_AbstractSubmittable
{

    protected function actionShow()
    {
        $model = array(
            'templateTitle' => V::_('Log in'),
            'user_name' => '',
            'message' => '',
            'errorcode' => 0,
        );
        return array('login', $model);
    }

    protected function actionSubmit()
    {
        $user_name = $_POST['user_name'];
        $password = $_POST['password'];
        $backlink = getBacklink();

        $userDao = new Dao_UserDao();
        $user = $userDao->findUserByName($user_name);
        if ($user) {
            if ($user->checkPassword($password)) {
                $_SESSION['user_id'] = $user->id;
                return array("redirect:$backlink", NULL);
            } else {
                $message = V::_('Invalid password');
                $errorcode = 2;
            }
        } else {
            $message = V::_('Invalid username');
            $errorcode = 1;
        }

        $model = array(
            'templateTitle' => V::_('Log in'),
            'user_name' => $user_name,
            'message' => $message,
            'errorcode' => $errorcode,
        );

        return array('login', $model);

    }

}
?>
