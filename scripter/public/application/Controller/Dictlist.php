<?php
class Controller_Dictlist extends Controller_AbstractSubmittable
{
    public function actionShow()
    {
        $rows = $this->getRows();
        $model = array(
            'templateTitle' => V::_('Dictionary list'),
            'rows' => $rows,
        );
        return array('dictlist', $model);
    }

    public function actionSubmit()
    {
        $db = Db::getDb();

        $stmt = $db->prepare("DELETE FROM dword_meaning WHERE dword_meaning_id = :dword_meaning_id");

        if (isset($_POST['dwordMeaningIds'])) {
            $dwordMeaningIds = intvals($_POST['dwordMeaningIds']);
        } else {
            $dwordMeaningIds = array();
        }

        foreach ($dwordMeaningIds as $dwordMeaningId) {
            $this->checkedDelete($stmt, $dwordMeaningId);
        }

        return $this->actionShow();
    }

    private function getRows()
    {
        $db = Db::getDb();
        $stmt = $db->query(<<<EOT
SELECT
    dw.dword_id AS dword_id,
    dw.word AS dict_word,
    dwm.dword_meaning_id AS dword_meaning_id,
    dwm.meaning_txt AS meaning,
    COUNT(sw.dword_meaning_id) AS occurences
FROM
    dword AS dw
    LEFT OUTER JOIN dword_meaning AS dwm
        ON dwm.dword_id = dw.dword_id
    LEFT OUTER JOIN sword AS sw
        ON sw.dword_meaning_id = dwm.dword_meaning_id
GROUP BY
    dict_word,
    meaning
ORDER BY
    dict_word,
    meaning
EOT
        );
        $rows = $stmt->fetchAll();

        return $rows;
    }

    private function checkedDelete($stmt, $dwordMeaningId)
    {
        $usage = $this->usage($dwordMeaningId);
        if (!$usage) {
            $stmt->bindParam(':dword_meaning_id', $dwordMeaningId);
            $stmt->execute();
        } else {
            throw new Exception("Inconsistent deletion: meaning #$dwordMeaningId is used by some sword occurences");
        }

    }

    private function usage($dwordMeaningId)
    {
        $db = Db::getDb();
        $stmt = $db->prepare(<<<EOT
SELECT sword_id
FROM sword
WHERE dword_meaning_id = :dword_meaning_id
EOT
        );
        $stmt->bindParam(':dword_meaning_id', $dwordMeaningId);
        $stmt->execute();
        $usage = $stmt->fetch(PDO::FETCH_ASSOC);

        if (DEBUG && is_array($usage)) asHtmlTable(array($usage), "Meaning #$dwordMeaningId is used by following swords:");
        if (DEBUG) var_dump($usage);

        return $usage;
    }
}
?>
