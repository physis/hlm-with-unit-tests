<?php

class Controller_Storyview
{
    public function action()
    {
        $scriptId = $_GET['script_id'];
        Check::checkIsset($scriptId);
        
        $db = Db::getDb();
        $st = $db->prepare( <<<EOT
SELECT
    sc.script_id,
    title,
    se.sentence_id,
    is_deleted,
    se.meaning_txt AS sentence_meaning,
    se.order_no AS sentence_order,
    sw.sword_id,
    sw.word AS word,
    word_suffix,
    sw.order_no AS sword_order,
    sw.dword_id,
    dw.word AS dict_word,
    sw.dword_meaning_id AS actual_meaning_id,
    grammar_form,
    origin_txt,
    stylistics_txt,
    spelling_txt,
    context_txt
FROM
    script sc JOIN sentence se
        ON se.script_id = sc.script_id
    JOIN sword sw
        ON sw.sentence_id = se.sentence_id
    LEFT OUTER JOIN dword dw
        ON dw.dword_id = sw.dword_id
WHERE
    sc.script_id = ?
ORDER BY se.order_no, sw.order_no
EOT
        );
        $st->execute(array($scriptId));

        $sentences = array();
        $currentSentence = NULL;
        $lastSentenceId = NULL;
        while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
            if ($row['sentence_id'] != $lastSentenceId) {
                $lastSentenceId = $row['sentence_id'];
                if (isset($currentSentence)) {
                    $sentences[] = $currentSentence;
                }
                $currentSentence = array();
            }
            $currentSentence[] = $row;
        }
        $sentences[] = $currentSentence;
/*
        $st = $db->prepare("select swm.sword_id, sword_meaning_id, meaning"
                . " from sentence s join sword sw"
                . "     on s.sentence_id = sw.sentence_id"
                . "   join sword_meaning swm"
                . "     on sw.sword_id = swm.sword_id"
                . " where script_id = ?"
                . " order by sword_id, sword_meaning_id");
        $st->execute(array($scriptId));
        $swordMeanings = $st->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP);

*/

        $join = <<<EOT
SELECT
    sword_id,
    dwm.dword_meaning_id,
    dwm.meaning_txt AS meaning
FROM
    sword sw JOIN sentence s
        ON s.sentence_id = sw.sentence_id
    JOIN dword dw
        ON dw.dword_id = sw.dword_id
    JOIN dword_meaning dwm
        ON dwm.dword_id = dw.dword_id
WHERE
    script_id = :script_id
ORDER BY
    sword_id,
    dword_meaning_id
EOT
        ;
        $st = $db->prepare($join);
        $st->bindParam(":script_id", $scriptId);
        $st->execute();
        $swordMeanings = $st->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP);
        if (DEBUG) asGroupedTables($swordMeanings, "sword_id");
        foreach ($sentences as $i => &$sentence) {
            foreach ($sentence as &$sword) {
                $swordId = $sword['sword_id'];
                $sword['meanings'] = isset($swordMeanings[$swordId]) ? $swordMeanings[$swordId] : array();                
            }
            if (DEBUG) asHtmlTable($sentence, "Sentence $i");
        }
        
        $model = array('templateTitle' => V::_('Story'),
                'sentences' => $sentences);
        return array('storyview', $model);
    }
    
    
}
