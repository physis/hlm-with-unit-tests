<?php

class Domain_User
{
    public $id;
    public $name;
    public $password;
    
    public function checkPassword($password)
    {
        return $this->password === $password;
    }
}
