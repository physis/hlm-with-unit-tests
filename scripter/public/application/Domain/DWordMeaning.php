<?php

class Domain_DWordMeaning
{
    public $id;
    public $dwordId;
    public $meaningTxt;

    public function __construct($id, $dwordId, $meaningTxt)
    {
        $this->id = $id;
        $this->dwordId = $dwordId;
        $this->meaningTxt = $meaningTxt;
    }
}
