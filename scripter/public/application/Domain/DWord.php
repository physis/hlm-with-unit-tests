<?php

class Domain_DWord
{
    public $id;
    public $word;

    public function __construct($dwordId, $word)
    {
        $this->id = $dwordId;
        $this->word = $word;
    }
}
