<?php

class Domain_SWord
{
    public $id;
    public $sentenceId;
    public $word;
    public $wordSuffix;
    public $dwordId;
    public $dwordMeaningId;
    public $grammarForm;
    public $originTxt;
    public $stylisticsTxt;
    public $spellingTxt;
    public $contextTxt;
    public $orderNo;
    public $sourceId;
    
    public $dword; // type Domain_DWord

    public function __get($name) // magic method for getting non-existing properties
    {
        if ($name === 'dwordTxt') {
            return $this->dword->word;
        }
    }
}