<script>
$(function() {
    linkIcon('a.sentenceedit', 'ui-icon-pencil');
    linkIcon('a.sentenceappend', 'ui-icon-plus');
    linkIcon('a.sentencedel', 'ui-icon-minus');
    linkIcon('a.audioedit', 'ui-icon-volume-on');

    function linkIcon(linkSelector, primaryIcon) {
        $(linkSelector).button({
            icons: { primary: primaryIcon },
            text: false
        });
    }
});
</script>
