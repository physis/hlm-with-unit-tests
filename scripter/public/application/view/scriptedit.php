<form method="POST">

    <table>
        <tbody>
            <tr>
                <td><?php echo V::_H('Title') ?>:</td>
                <td>
                    <input name="script_id" type="hidden" value="<?php echo $scriptId;?>"/>
                    <input name="title" value="<?php echo V::H($title);?>"/>
                    <?php if (isset($errors['title'])) echo $errors['title'];?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="<?php echo V::_H('Save') ?>"/></td>
            </tr>
            <tr>
                <td valign="top"><?php echo V::_H('Story') ?>:</td>
                <td>
<?php
if (isset($registeredSentences) && ($n = count($registeredSentences)) > 0) {
?>
                    <table border="1">
                        <caption>
                            <?php echo V::_FH('This script has already been created containing %d sentences', array($n)) ?>:
                        </caption>
<?php
    foreach ($registeredSentences as $registeredSentence) {
        $sentence_id = $registeredSentence['sentence_id'];
        $sentenceAsWordList = $registeredSentence['sentenceAsWordList'];
?>
                        <tr>
                            <td>
                                <?php echo $sentence_id ?>
                            </td>
                            <td>
                                <?php echo V::H(implode($sentenceAsWordList)) ?>
                            </td>
                            <td>
                                <a class="sentenceedit" href="sentenceedit?sentence_id=<?php echo $sentence_id;?>">
                                    <?php echo V::_H('Edit') ?>
                                </a>
                            </td>
                            <td>
                                <a class="sentenceappend" href="sentenceappend?after_sentence_id=<?php echo $sentence_id;?>">
                                    <?php echo V::_H('Append sentence') ?>
                                </a>
                            </td>
                            <td>
                                <a class="sentencedel" href="sentencedel?sentence_id=<?php echo $sentence_id;?>">
                                    <?php echo V::_H('Delete sentence') ?>
                                </a>
                            </td>
                            <td>
                                <a class="audioedit" href="audioedit?sentence_id=<?php echo $sentence_id;?>">
                                    <?php echo V::_H('Audio') ?>
                                </a>
                            </td>
                        </tr>
<?php
    }
?>
                    </table>
                    <a href="storyview?script_id=<?php echo $scriptId ?>">
                        <?php echo V::_H('Preview') ?>
                    </a>
                    <br>
                    <a href="scripttolect?script_id=<?php echo $scriptId ?>">
                        <?php echo V::_H('Promote to lector') ?>
                    </a>
                    <br>
                    <a href="scriptdel?script_id=<?php echo $scriptId ?>">
                        <?php echo V::_H('Delete script') ?>
                    </a>
<?php
} else {
    if (!isset($story)) {
        $story = '';
    }
?>
                    <?php if (isset($errors['story'])) echo $errors['story'];?>
                    <br/>
                    <textarea name="story" cols="60" rows="40"><?php echo $story;?></textarea>
<?php
}
?>
                </td>
            </tr>
        </tbody>
    </table>

</form>
