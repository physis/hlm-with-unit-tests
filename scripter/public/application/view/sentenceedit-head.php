<?php
function getDict($swords)
{
    $dict = array();
    foreach ($swords as $sword) {
        $meanings = array();
        foreach ($sword->meanings as $meaning) {
            $meanings[] = $meaning->meaningTxt;
        }
        $dict[$sword->dword->word] = $meanings;
    }
    return $dict;
}

function wordArray($swords)
{
    $words = array();
    foreach ($swords as $sword) {
        $words[] = $sword->word;
    }
    return $words;
}

?>

<script>
$(function() {

    window.dict = <?php echo json_encode(getDict($swords)) ?>;
    window.words = <?php echo json_encode(wordArray($swords)) ?>;
    
    function siblingCtl(ctl, otherCtlKind) {
        var otherName = ctl.name.replace(/\[\w+\]$/, '[' + otherCtlKind + ']'); // e.g. swords[12][foo] -> swords[12][otherCtlKind]
        return ctl.form.elements[otherName];
    }

    function indexOfCtl(ctl) {
        return ctl.name.match(/^swords\[(\d+)\]/)[1];
    }
    
    function meaningAutoCompleteFun(ctl) {
        return function(request, response) {
            var word = siblingCtl(ctl, 'dwordTxt').value;
            response(dict[word]);
        }
    }

    $("input.attr-act_meaning").each(function() {
        $(this).autocomplete({
            source: meaningAutoCompleteFun(this),
            minLength: 0
        });
    });
    $("input.attr-act_meaning").focus(function() {
        $(this).autocomplete('search');
    });

    $("input.attr-dwordTxt").blur(function(event) {
        var dictCtl = event.target;
        var meaningCtlName = dictCtl.name.replace('dwordTxt', 'act_meaning');
        var meaningCtl = dictCtl.form.elements[meaningCtlName];
        meaningCtl.value = '';
        var word = dictCtl.value;
        if (word.length && !dict[word]) { // non-empty and not known in client-side dictionary
            $.getJSON("dict", { word: word }, function(data) {
                if (data && data.length) { // do not store words with no meaning
                    dict[word] = data;
                }
            });
        }
    });

    $("input.copy-dwordTxt").click(function(event) {
        var dictCtl = siblingCtl(event.target, 'dwordTxt');
        dictCtl.value = words[indexOfCtl(dictCtl)];
        dictCtl.focus();
    });
    $("input.copy-dwordTxt-lower").click(function(event) {
        var dictCtl = siblingCtl(event.target, 'dwordTxt');
        dictCtl.value = words[indexOfCtl(dictCtl)].toLowerCase();
        dictCtl.focus();
    });

    var grammarHints = ['verb active present simple',
                        'verb active present continuous',
                        'verb active present perfect',
                        'verb active present perfect continuous',
                        'verb active past simple',
                        'verb active past continuous',
                        'verb active past perfect',
                        'verb active past perfect continuous',
                        'verb active future simple',
                        'verb active future continuous',
                        'verb active future perfect',
                        'verb active future perfect continuous',
                        'verb passive present simple',
                        'verb passive present continuous',
                        'verb passive present perfect',
                        'verb passive past simple',
                        'verb passive past continuous',
                        'verb passive past perfect',
                        'verb passive future simple',
                        'verb passive future continuous',
                        'verb passive future perfect',
                        'article',
                        'definite article',
                        'indefinite article',
                        'auxiliary verb',
                        'adjective',
                        'adverb',
                        'comparative',
                        'conjunction',
                        'interjection',
                        'noun',
                        'noun plural',
                        'plural',
                        'past participle',
                        'prefix',
                        'preposition',
                        'present participle',
                        'pronoun',
                        'proper noun',
                        'singular',
                        'suffix',
                        'superlative',
                        'infinitive',
                        'numeral',
                        'possessive'];

    function grammarAutoCompleteFun(ctl) {
        return function(request, response) {
            // matches if term substrings match word prefixes
            // e.g. term="loip" matches for "lorem ipsum dolor sit amet"
            var termRegexp = new RegExp('^' + request.term.split('').join('(|.*\\b)' /* matches empty string or anything until a word beginning */));
            var resp = [];
            for (var i=0; i<grammarHints.length; ++i) {
                if (termRegexp.test(grammarHints[i])) {
                    resp.push(grammarHints[i]);
                }
            }
            response(resp);
        };
    }

    $("input.attr-grammarForm").each(function() {
        $(this).autocomplete({
            source: grammarAutoCompleteFun(this)
        });
    });

});

</script>
