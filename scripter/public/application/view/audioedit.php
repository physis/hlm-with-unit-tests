<?php $translation = array(
    'en_UK' => V::_('British'),
    'en_US' => V::_('American'),
);
?>
<script type="text/javascript" src="<?php echo HREF_BASE;?>/js/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
<?php
foreach ($map as $locale => $url) {
    $withoutExtension = withoutExtension($url);
?>
    $("#<?php echo $locale;?>").jPlayer({
        ready: function () {
            $(this).jPlayer("setMedia", {
                mp3: "<?php echo $withoutExtension ?>.mp3"
            });
        },
        swfPath: "<?php echo HREF_BASE;?>/js/jplayer",
        supplied: "mp3"
    });
<?php
}
?>
});
</script>
<p><?php echo V::_FH('Uploading audio file for sentence of id %s', array($sentence_id)) ?></p>

<form method="post" enctype="multipart/form-data">
    <input type="hidden" name="sentence_id" value="<?php echo $sentence_id;?>"/>
    <table>
<?php
foreach ($translation as $locale => $name) {
    if (isset($map[$locale])) {
        $url = $map[$locale];
        $extension = extension($url);
?>
        <tr>
            <td>
                <a href="<?php echo $url;?>">
                    <?php echo V::_FH('Listen in %s', array($name)) ?>
                </a>
                <button type="button" onclick="$('#<?php echo $locale;?>').jPlayer('play')">jPlay</button>
                <div id="<?php echo $locale;?>" class="jp-jplayer"></div>
            </td>
            <td>
                <input type="checkbox" name="<?php echo implode('__', array($locale, $extension));?>"/>
                <?php echo V::_H('Delete');?>
            </td>
            <td><!-- Now error messages yet --></td>
        </tr>
<?php
    } else {
?>
        <tr>
            <td><?php echo V::_FH('Upload in %s', array($name)) ?></td>
            <td><input type="file" name="<?php echo $locale;?>"/></td>
            <td><?php if (isset($errors[$locale])) echo $errors[$locale];?></td>
        </tr>
<?php
    }
}
?>
    </table>
    <input type="submit" name="submit" value="<?php echo V::_H('Submit') ?>"/>
</form>
