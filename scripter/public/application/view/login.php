<form method="post">
    <table>
        <tr>
            <td>
                <?php echo V::_H('Username') ?>:
            </td>
            <td>
                <input type="text" name="user_name" value="<?php echo V::H($user_name);?>"/>
            </td>
            <td>
                <?php if ($errorcode == 1) echo V::H($message); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo V::_H('Password') ?>:
            </td>
            <td>
                <input type="password" name="password"/>
            </td>
            <td>
                <?php if ($errorcode == 2) echo V::H($message); ?>
            </td>
        </tr>
    </table>
    <input type="submit" name="logged_in" value="<?php echo V::_H('Log in') ?>"/>
</form>
