<?php if (DEBUG) var_dump($_POST);?>
<form method="POST">
    <table>
        <thead>
            <tr>
                <?php if (DEBUG) echo '<th>dwordId</th>';?>
                <th><?php echo V::_H('Dictionary word');?></th>
                <?php if (DEBUG) echo '<th>dwordMeaningId</th>';?>
                <th><?php echo V::_H('Meaning');?></th>
                <th><?php echo V::_H('Occurences');?></th>
                <th><?php echo V::_H('Delete?');?></th>
            </tr>
        </thead>
        <tbody>
<?php
$disableMeaningDeletion = true;
foreach ($rows as $row) {
    $dwordId = $row['dword_id'];
    $dwordMeaningId = $row['dword_meaning_id'];
    $occurences = $row['occurences'];
?>
            <tr>
                <?php if (DEBUG) echo '<td>' . $dwordId . '</td>';?>
                <td><?php echo $row['dict_word'];?></td>
                <?php if (DEBUG) echo '<td>' . $dwordMeaningId . '</td>';?>
                <td><?php echo $row['meaning'];?></td>
                <td><?php echo $occurences;?></td>
                <td>
<?php
    if ($occurences == 0 && $dwordMeaningId != '') {
        $disableMeaningDeletion = false;
?>
                    <input type="checkbox" name="dwordMeaningIds[]" value="<?php echo $dwordMeaningId;?>"/>
<?php
    }
?>
                </td>
            </tr>
<?php
}
?>
        </tbody>
    </table>
    <br/>
    <input type="submit" name="delete" value="Delete" <?php if ($disableMeaningDeletion) echo 'disabled';?>/>
</form>
