<div style="text-align: center">

<?php echo V::_H('This will delete the whole sentence, including all the data you have added to it. Are you sure?') ?>

<p style="border: 1px dashed black"><?php echo V::H($sentenceTxt) ?></p>

<form method="post">
  <input type="hidden" name="sentence_id" value="<?php echo $sentenceId ?>">
  <input type="submit" value="<?php echo V::_H('Delete sentence') ?>">
  <input type="button" value="<?php echo V::_H('Cancel') ?>" onclick="history.back()">
</form>

</div>
