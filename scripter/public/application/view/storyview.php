
<div id="sword-bar">
    <div class="sword-word"></div>
    <ul>
        <li>
            <button id="sword-dword" class="icon"><?php echo V::_H('Dictionary word')?></button>
        </li>
        <li>
            <button id="sword-origin" class="icon"><?php echo V::_H('Origin')?></button>
        </li>
        <li>
            <button id="sword-stylistics" class="icon"><?php echo V::_H('Stylistics')?></button>
        </li>
        <li>
            <button id="sword-spelling" class="icon"><?php echo V::_H('Spelling')?></button>
        </li>
        <li>
            <button id="sword-context" class="icon"><?php echo V::_H('Context')?></button>
        </li>
        <li>
            <button id="sentence-meaning" class="icon"><?php echo V::_H('Sentence meaning')?></button>
        </li>
    </ul>
</div>

<div id="sword-detail"></div>

<div id="sword-meanings" title="<?php echo V::_("Meanings")?>"></div>

<div id="story">
<?php foreach ($sentences as $sentence) { ?>
<div id="sentence-<?php echo $sentence[0]['sentence_id']?>" class="sentence">
<?php   foreach ($sentence as $sword) { ?>
<span id="swid-<?php echo $sword['sword_id']?>" class="sword"><?php echo V::H($sword['word']) ?></span><?php echo nl2br(V::H($sword['word_suffix'])) ?>
<?php   } ?>
</div>
<?php } ?>
</div>


