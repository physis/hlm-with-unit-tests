<div style="text-align: center">

<?php echo V::_H('This will delete the whole script, including all the data you have added to it. Are you sure?') ?>

<form method="post">
  <input type="hidden" name="script_id" value="<?php echo $scriptId ?>">
  <input type="submit" value="<?php echo V::_H('Delete story') ?>">
  <input type="button" value="<?php echo V::_H('Cancel') ?>" onclick="history.back()">
</form>

</div>
