<div style="text-align: center">

<?php echo V::_H('This will close the initial editing phase. Are you sure?') ?>

<form method="post">
  <input type="hidden" name="script_id" value="<?php echo $scriptId ?>">
  <input type="submit" value="<?php echo V::_H('Promote script') ?>">
  <input type="button" value="<?php echo V::_H('Cancel') ?>" onclick="history.back()">
</form>

</div>
