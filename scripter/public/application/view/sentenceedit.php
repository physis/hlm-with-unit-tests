<p>
    <?php echo V::_H('Sentence ID') ?>: <?php echo $sentence_id ?>
</p>
<form method="post">
    <input type="hidden" name="sentence_id" value="<?php echo $sentence_id;?>"/>
    <?php echo V::_H('Meaning') ?>:
    <br/>
    <textarea name='meaning_txt' rows="3" cols="60"><?php echo V::H($meaning) ?></textarea>
<?php
    if ($source) {
        echo V::H($source['meaning']);
    }
?>
    <hr/>
<?php
    $wordCount = count($swords);
    for ($i=0; $i<$wordCount; ++$i) { // Write ID as hidden variable for each sentence_word
?>
            <input type="hidden" name='<?php echo "swords[$i][sword_id]" ?>'
                    value="<?php echo $swords[$i]->id ?>">
<?php
    }
?>
    <table>
<?php
function echoSwordMeaningsEditor($swords, $source)
{
    $attrName = 'act_meaning';
?>
        <tr>
            <th colspan="2" style="padding-top: 1em"><?php echo V::_H("Meanings") ?></th>
        </tr>
<?php
    $wordCount = count($swords);
    for ($i=0; $i<$wordCount; ++$i) {
        $sword = $swords[$i];
?>
        <tr>
            <td><?php echo V::H($sword->word) ?></td>
            <td>
<?php
                $meaningCount = count($sword->meanings);
                $meanings = array();
                $actMeaning = '';
                for ($j=0; $j<$meaningCount; ++$j) {
                    $meaning = $sword->meanings[$j];
                    if ($sword->dwordMeaningId === $meaning->id) {
                        $actMeaning = $meaning->meaningTxt;
                    }
                    $meanings[] = $meaning->meaningTxt;
                }
?>
                <input id='<?php echo "swords_${i}_act_meaning"?>' name='<?php echo "swords[$i][$attrName]" ?>'
                        value='<?php echo V::H($actMeaning) ?>'
                        class='<?php echo "attr-$attrName" ?>'>
            </td>
<?php if ($source) { ?>
            <td>
              <?php echo V::H($source['swordsMap'][$sword->sourceId]->dwordTxt) ?>
            </td>
<?php } ?>
            </tr>
        
<?php
    }
}
?>
<?php
function echoSwordAttributeEditor($swords, $source, $title, $attrName)
{
    $wordCount = count($swords);
?>
        <tr>
            <th colspan="2" style="padding-top: 1em"><?php echo V::H($title) ?></th>
        </tr>
<?php
    for ($i=0; $i<$wordCount; ++$i) {
        $sword = $swords[$i];
?>
        <tr>
            <td><?php echo V::H($sword->word) ?></td>
            <td>
                <input name='<?php echo "swords[$i][$attrName]" ?>'
                        value="<?php echo V::H($sword->$attrName) ?>"
                        class='<?php echo "attr-$attrName" ?>'>
<?php if ($attrName === 'dwordTxt') { ?>
        <input name='<?php echo "swords[$i][copy]" ?>' type='button' value='<?php echo V::_H('=') ?>' class='copy-dwordTxt'>
<?php     if (preg_match("/[A-Z]/", $sword->word)) { ?>
            <input name='<?php echo "swords[$i][copyc]" ?>' type='button' value='<?php echo V::_H('=abc') ?>' class='copy-dwordTxt-lower'>
<?php     }?>
<?php } ?>
            </td>
<?php if ($source) { ?>
            <td>
              <?php echo V::H($source['swordsMap'][$sword->sourceId]->$attrName) ?>
            </td>
<?php } ?>
        </tr>
<?php
    }
}
    echoSwordAttributeEditor($swords, $source, V::_("Dictionary form"), 'dwordTxt');
    echoSwordMeaningsEditor($swords, $source);
    echoSwordAttributeEditor($swords, $source, V::_("Grammatical form"), 'grammarForm');
    echoSwordAttributeEditor($swords, $source, V::_("Origin"), 'originTxt');
    echoSwordAttributeEditor($swords, $source, V::_("Stylistics"), 'stylisticsTxt');
    echoSwordAttributeEditor($swords, $source, V::_("Spelling"), 'spellingTxt');
    echoSwordAttributeEditor($swords, $source, V::_("Context"), 'contextTxt');
?>
    </table>
    <hr/>
    <input type="submit" value="<?php echo V::_H('Update') ?>"/>
</form>
