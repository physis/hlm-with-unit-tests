<table>
    <thead>
        <tr>
            <th><?php echo V::_H('Title') ?></th>
            <th><?php echo V::_H('Status') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php $statuses = VLookup::statuses(); ?>
    <?php foreach ($rows as $row) { ?>
        <tr>
            <td><a href="scriptedit?script_id=<?php echo $row->id ?>">
                <?php echo $row->title ?>
            </a></td>
            <td>
                <?php echo V::H($statuses[$row->statusId]) ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<p>
    <a href="scriptnew"><?php echo V::_H('New script') ?></a>
</p>
