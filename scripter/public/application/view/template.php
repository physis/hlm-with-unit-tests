<?php
    $templateTitleH = V::H($templateTitle);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $templateTitleH ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo HREF_BASE."/style/hlm.css" ?>"/>
        <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/ui-lightness/jquery-ui.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
        <?php if (isset($templateHead)) { require $templateHead; } ?>
    </head>
    <body>
        <div class="template-head">
           <h1><?php echo $templateTitleH ?></h1>
        </div>
        <table class="template-main">
            <tr>
                <?php if (isset($_SESSION['user_id'])) { ?>
                    <td class="template-nav"><?php require "nav.php" ?></td>
                <?php } ?>
                <td class="template-content"><?php require $templateContent ?></td>
            </tr>
        </table>
    </body>
</html>
