<style>
#sword-bar {
    background-color: #906090;
}

.sword-word {
    height: 1em;
    text-align: center;
}

#sword-bar ul {
    padding: 0;
}

#sword-bar li {
    display: inline;
}

#sword-detail {
    margin-bottom: 1em;
    padding: 0.5em;
    border: 1px solid black;
    border-radius: 10px;
}

.sentence-selected {
    background-color: #eeeeee;
}

.sword-selected {
    border: 1px solid black;
    background-color: #cccccc;
}

.actualmeaning.highlight {
    font-weight: bold;
}

#sword-bar button {
    width: 64px; height: 64px;
}

#sword-bar .ui-button-icon-primary {
    width: 64px;
    height: 64px;
    margin-left: -32px;
    margin-top: -32px;
}

#sword-bar .ui-icon-dword {
    background-image: url(<?php echo HREF_BASE."/img/bagoly64.jpg" ?>)
}

#sword-bar .ui-icon-origin {
    background-image: url(<?php echo HREF_BASE."/img/csillag64.jpg" ?>)
}

#sword-bar .ui-icon-stylistics {
    background-image: url(<?php echo HREF_BASE."/img/varazslo64.jpg" ?>)
}

#sword-bar .ui-icon-spelling {
    background-image: url(<?php echo HREF_BASE."/img/eger64.jpg" ?>)
}

#sword-bar .ui-icon-context {
    background-image: url(<?php echo HREF_BASE."/img/majom64.jpg" ?>)
}

#sentence-meaning .ui-icon-sentence-meaning {
    background-image: url(<?php echo HREF_BASE."/img/varazslo64.jpg" ?>)
}

</style>
<script>

var sentences = {
<?php
    foreach ($sentences as $sentence) {
        $id = $sentence[0]['sentence_id'];
        $meaningJs = json_encode($sentence[0]['sentence_meaning']);
        echo "$id: {meaning: $meaningJs},\n";
    }
?>
};

var swords = {
<?php
    foreach ($sentences as $sentence) {
        foreach ($sentence as $sword) {
            $swordId = $sword['sword_id'];
            $word = $sword['word'];
            $dictWord = $sword['dict_word'];
            $origin = $sword['origin_txt'];
            $stylistics = $sword['stylistics_txt'];
            $spelling = $sword['spelling_txt'];
            $context = $sword['context_txt'];
            $meanings = array();
            $actualMeaningIdx = 'null';
            foreach ($sword['meanings'] as $idx => $meaning) {
                $meanings[] = $meaning['meaning'];
                if ($meaning['dword_meaning_id'] == $sword['actual_meaning_id']) {
                    $actualMeaningIdx = $idx;
                }
            }
            $meaningsJs = json_encode($meanings);
            echo "$swordId: {word:'$word', dictWord:'$dictWord', origin:'$origin',"
                    ."stylistics:'$stylistics', spelling:'$spelling', context:'$context',"
                    ."meanings: $meaningsJs, actualMeaningIdx: $actualMeaningIdx"
                    ."},\n";
        }
    }
?>
};

function swid(elem) {
    return elem.id.replace("swid-", "");
}

function sentenceId(elem) {
    return elem.id.replace("sentence-", "");
}

$(function() {

    var selectedWord = null;
    var selectedSentence = null;


    function clearWordSelection() {
        if (selectedWord) {
            $(selectedWord).removeClass("sword-selected");
            selectedWord = null;
            $("#sword-bar .sword-word").text("");
        }
    }

    function selectWord(elem) {
        clearWordSelection();
        $(elem).addClass("sword-selected");
        selectedWord = elem;
        $("#sword-bar .sword-word").text($(elem).text());
        syncIcons();
        //showMeanings();
    }

    function syncIcons() {
        var id = swid(selectedWord);
        var sword = swords[id];
        $("#sword-dword").button("option", "disabled", sword.dictWord == "");
        $("#sword-origin").button("option", "disabled", sword.origin == "");
        $("#sword-stylistics").button("option", "disabled", sword.stylistics == "");
        $("#sword-spelling").button("option", "disabled", sword.spelling == "");
        $("#sword-context").button("option", "disabled", sword.context == "");
    }

    function buildMeanings($target, swordId) {
        var sword = swords[swordId];
        $target.html('<ul/>');
        $.each(sword.meanings, function(idx, val) {
            var $li = $('<li/>', {text: val});
            if (idx == sword.actualMeaningIdx) $li.addClass("actualmeaning");
            $target.find("ul").append($li);
        });
        return $target;
    }

    function tooltipContent() {
        var id = swid(this);
        return buildMeanings($("<div/>"), id).html();
    }

    $("#story").on("click", ".sword", function() {
        selectWord(this);
    });

    $("#story").on("click", "div.sentence", function() {
        if (this != selectedSentence) {
            if (selectedSentence) $(selectedSentence).removeClass("sentence-selected");
            $(this).addClass("sentence-selected");
            selectedSentence = this;
            if (!$.contains(this, selectedWord)) {
                clearWordSelection();
            }
        }
    });

    $("#story").on("dblclick", ".sword", function() {
        $(".actualmeaning").addClass("highlight");
    });

    $("#story").on("mouseleave", ".sword", function() {
        $(".actualmeaning").removeClass("highlight");
    });

    $("#sword-dword").button({
        text: false,
        icons: { primary: 'ui-icon-dword' }
    });
    $("#sword-origin").button({
        text: false,
        icons: { primary: 'ui-icon-origin' }
    });
    $("#sword-stylistics").button({
        text: false,
        icons: { primary: 'ui-icon-stylistics' }
    });
    $("#sword-spelling").button({
        text: false,
        icons: { primary: 'ui-icon-spelling' }
    });
    $("#sword-context").button({
        text: false,
        icons: { primary: 'ui-icon-context' }
    });
    $("#sentence-meaning").button({
        text: false,
        icons: { primary: 'ui-icon-sentence-meaning' }
    });

    $("#sword-dword").click(function() {
        var id = swid(selectedWord);
        $("#sword-detail").text(swords[id].dictWord);
    });
    $("#sword-origin").click(function() {
        var id = swid(selectedWord);
        $("#sword-detail").text(swords[id].origin);
    });
    $("#sword-stylistics").click(function() {
        var id = swid(selectedWord);
        $("#sword-detail").text(swords[id].stylistics);
    });
    $("#sword-spelling").click(function() {
        var id = swid(selectedWord);
        $("#sword-detail").text(swords[id].spelling);
    });
    $("#sword-context").click(function() {
        var id = swid(selectedWord);
        $("#sword-detail").text(swords[id].context);
    });
    $("#sentence-meaning").click(function() {
    	$("#sword-detail").text(sentences[sentenceId(selectedSentence)].meaning);
    });

    $("#story").tooltip({ items: ".sword", content: tooltipContent });
});

</script>
