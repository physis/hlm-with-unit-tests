<?php

/**
 * Utility class for HTML views.
 * Supports escaping and locale-specific messages.
 */
class V
{

    public static function H($str) // H as "HTML escape"
    {
        return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
    }
    
    public static function _($msg)
    {
        global $message;
        $msg = isset($message[$msg]) ? $message[$msg] : $msg;
        return $msg;
    }

    public static function _F($msg, $args) // F as "format"
    {
        return vsprintf(V::_($msg), $args);
    }

    public static function _H($msg)
    {
        return V::H(V::_($msg));
    }

    public static function _FH($msg, $args)
    {
        return V::H(V::_F($msg, $args));
    }

}

