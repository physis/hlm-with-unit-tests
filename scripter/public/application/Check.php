<?php

class Check
{

    public static function checkIsset(&$var, $msg='Variable not set')
    {
        if (!isset($var)) {
            Check::fail($msg);
        }
    }
    
    private static function fail($msg)
    {
        throw new Exception("Check failed: $msg");
    }
}

