<?php
    include 'utility.php';

    // define some global constants for application configuration
    define(APP_NAME, basename(__FILE__, '.php'));
    define(ROOT_DIR, dirname(__FILE__));
    define(APP_DIR, ROOT_DIR . '/application');
    define(HREF_BASE, dirname($_SERVER['SCRIPT_NAME']));
    define(VIEW_DIR, APP_DIR . '/view'); // directory for views
    define(AUDIO_DIR, ROOT_DIR . '/audio');
    define(AUDIO_DIR_HREF_BASE, HREF_BASE . '/audio');

    define(DB_HOST, '@DB_HOST@');
    define(DB_DB, '@DB_DB@');
    define(DB_USER, '@DB_USER@');
    define(DB_PWD, '@DB_PWD@');

    define(DEV_MODE, '@DEV_MODE@'=='true');
    define(DEBUG, '@DEBUG@'=='true');

    ini_set('display_errors', 1);
    error_reporting(E_ALL | E_STRICT);

    set_include_path(str_replace('{public}', ROOT_DIR, '@INCLUDE_PATH_PREPEND@') . PATH_SEPARATOR . get_include_path());

    // set up class autoloader, see spl_autoload_register
    function hierAutoload($className)
    {
        $path = str_replace('_', '/', $className);
        // e.g. class Foo_Bar -> Foo/Bar.pap
        require "$path.php";
    }
    spl_autoload_register('hierAutoload');

    // undo magic quotes; for more complete solutions, see http://php.net/get_magic_quotes_gpc
    if (get_magic_quotes_gpc()) {
        // see http://php.net/manual/en/function.stripslashes.php#101338
        $unmagic_quotes_fun = create_function('&$val', '$val = stripslashes($val);');
        array_walk_recursive($_GET, $unmagic_quotes_fun);
        array_walk_recursive($_POST, $unmagic_quotes_fun);
    }

    function loginInterceptor()
    {
        session_start();
        $controllerName = getControllerName(); // utility.php
        if ($controllerName == 'login') {
            return TRUE;
        } else {
            $loggedIn = isset($_SESSION['user_id']);
            if ($loggedIn) {
                $user_id = $_SESSION['user_id'];
                return TRUE;
            } else {
                setBacklink();
                // header("Location: $controllerName"); // redirect
                echo "redirect to <a href=\"login\">login</a>"; // login will use backlink set by `setBacklink()' call
                return FALSE;
            }
        }
    }

    // messages setup
    $locale = 'hu_HU';
    include APP_DIR . "/message/messages.$locale.php";

    $views = array(
        'sentenceedit' => array(
            'head' => 'sentenceedit-head'
        ),
        'storyview' => array(
            'head' => 'storyview-head'
        ),
        'scriptedit' => array(
            'head' => 'scriptedit-head'
        )
    );
    
    // load and activate MVC framework
    $mvc = new Mvc($views);
    $mvc->addInterceptorPre('loginInterceptor');
    $mvc->dispatch();
