<?php
function getBacklink()
{
    if (isset($_SESSION['backlink'])) {
        $backlink = $_SESSION['backlink'];
    } else {
        $backlink = 'scriptlist';
    }
    return $backlink;
}

function setBacklink()
{
    $currentLink = getCurrentLink();
    $_SESSION['backlink'] = $currentLink;
}

function getCurrentlink()
{
    $currentLink = getControllerName();
    if ($_GET) {
        $currentLink .= '?' . getGet();
    }
    return $currentLink;
}

function getControllerName()
{
    $pathParts = explode('/', $_SERVER['PATH_INFO']); // copied from Mvc. php, TODO: share an aux function with it
    return $pathParts[1];
}

function getGET()
{
    $config = array();
    foreach ($_GET as $name => $value) {
        $config[] = "$name=$value";
    }
    return implode('&', $config);
}

// Aux functions that are used also in controllers

function hostScriptOf($sentence_id)
{
    $db = Db::getDb(); // // Db class maintains the same reference for each getting inside a http-request

        $select_sentence = $db->prepare('SELECT * FROM sentence WHERE sentence_id = :sentence_id');
        $select_sentence->bindParam(':sentence_id', $sentence_id);
        $select_sentence->execute();
        $sentence_rec = $select_sentence->fetch(PDO::FETCH_ASSOC);

        if ($sentence_rec) {
            $script_id = $sentence_rec['script_id'];
        } else {
            $script_id = NULL;
        }

    $db = NULL;

    return $script_id;
}

// Import / export `story' (`body' of script) into / out of the database

function exportStory_tripleArticulation($script_id) // on the analogy of Saussure's linguistic concept `double articulation'
{
    $db = Db::getDb(); // Db class maintains the same reference for each getting inside a http-request

    $select_script = $db->prepare("select * from script"
            . " where script_id = ?");
    $select_script->execute(array($script_id));
    $rec = $select_script->fetch();
    $title = $rec['title'];

    $select_sentences = $db->prepare('SELECT * FROM sentence WHERE script_id = :script_id ORDER BY order_no');
    $select_sentences->bindParam(':script_id', $script_id);
    $select_sentences->execute();

    $select_words = $db->prepare('SELECT * FROM sword WHERE sentence_id = :sentence_id ORDER BY order_no');
    $registeredSentences = array();
    while ($sentence_rec = $select_sentences->fetch(PDO::FETCH_ASSOC)) {
        $sentence_id = $sentence_rec['sentence_id'];
        $select_words->bindParam(':sentence_id', $sentence_id);
        $select_words->execute();

        $sentenceAsWordList = array();
        while ($word_rec = $select_words->fetch(PDO::FETCH_ASSOC)) {
            $wordProper = $word_rec['word'];
            $trailer = $word_rec['word_suffix'];
            $trailedWord = $wordProper . $trailer;
            $sentenceAsWordList[] = $trailedWord;
        }

        $registeredSentence = array('sentence_id' => $sentence_id, 'sentenceAsWordList' => $sentenceAsWordList);
        $registeredSentences[] = $registeredSentence;
    }

    $registeredScript = array('title' => $title, 'registeredSentences' => $registeredSentences);
    return $registeredScript;
}




/* INPORTSTORY AND ITS AUXILIARY FUNCTIONS */


function importStory($story, $scriptId)
{
    $sentences = splitToSentences($story);
    foreach ($sentences as $sentenceOrderNo => $sentence) {
        $sentenceBody = $sentence['body'];
        $sentencePunctuation = $sentence['punctuation'];
        insert_sentence($sentenceBody, $sentencePunctuation, $scriptId, $sentenceOrderNo, false);
    }

}

function incrementSentenceOrderNos($scriptId, $orderNo)
// make room for new sentence: increment all >= orderNo's
{
    $db = Db::getDb();

    $increment_orderNos = $db->prepare(<<<EOT
UPDATE sentence SET order_no = order_no + 1
WHERE script_id = :script_id AND order_no >= :order_no
EOT
    );
    $increment_orderNos->bindParam(':script_id', $scriptId);
    $increment_orderNos->bindParam(':order_no', $orderNo);
    $increment_orderNos->execute();
}

function insert_sentence($sentenceBody, $sentencePunctuation, $scriptId, $sentenceOrderNo, $renumber)
{
    $db = Db::getDb();

    if ($renumber) incrementSentenceOrderNos($scriptId, $sentenceOrderNo);

    $insert_sentence = $db->prepare(<<<EOT
INSERT INTO sentence ( script_id,  order_no)
       VALUES        (:script_id, :order_no)
EOT
    );

    $insert_sentence->bindParam(':script_id', $scriptId);
    $insert_sentence->bindParam(':order_no' , $sentenceOrderNo);

    $insert_sentence->execute();
    $sentenceId = $db->lastInsertId('sentence_id');

    insert_swords($sentenceBody, $sentencePunctuation, $sentenceId);

    return $sentenceId;

}


function insert_swords($sentenceBody, $sentencePunctuation, $sentenceId)
{
    $sWords = splitToWords($sentenceBody);
    $lastSWordOrderNo = count($sWords) - 1;

    $db = Db::getDb();
    $insert_sword = $db->prepare(<<<EOT
INSERT INTO sword ( sentence_id,  word,  word_suffix,  order_no)
       VALUES     (:sentence_id, :word, :word_suffix, :order_no)
EOT
    );
    $insert_sword->bindParam(':sentence_id', $sentenceId);

    foreach ($sWords as $sWordOrderNo => $sWord) {
        $sWordBody = $sWord['body'];
        $sWordTrailer = $sWord['punctuation'];
        if ($sWordOrderNo == $lastSWordOrderNo) {
            $sWordTrailer .= $sentencePunctuation;
        }

        $insert_sword->bindParam(':order_no', $sWordOrderNo);
        $insert_sword->bindParam(':word', $sWordBody);
        $insert_sword->bindParam(':word_suffix', $sWordTrailer);

        $insert_sword->execute();
    }

}




function sentenceToBodyAndPunctuation($sentenceTxt) // "A sentence..." => array("A sentence", "...")
{
    // almost the same regexp as in splitToSentences()
    $arr = preg_split('/([\.?!][^a-zA-Z]*)$/', $sentenceTxt, NULL, PREG_SPLIT_DELIM_CAPTURE);
    return array($arr[0], count($arr) > 1 ? $arr[1] : '');
}

function splitToSentences($story)
{
    // almost the same regexp as in sentenceToBodyAndPunctuation()
    $rough = pairedSplit('/([\.?!][^a-zA-Z]*)/', $story);
    return emptiless($rough);
}

function splitToWords($sentence)
{
    $rough = pairedSplit('/([^a-zA-Z]+)/', $sentence);
    return emptiless($rough);
}

function pairedSplit($pattern, $text)
{
    $roundRobin = preg_split($pattern, $text, -1, PREG_SPLIT_DELIM_CAPTURE);

    // Transpose `matrix':
    // ... transpose `out':
    $bodyColumn = array();
    $punctuationColumn = array();
    $isBody = TRUE;
    foreach ($roundRobin as $bodyOrPunctuation) {
        if ($isBody) {
            $bodyColumn[] = $bodyOrPunctuation;
        } else {
            $punctuationColumn[] = $bodyOrPunctuation;
        }
        $isBody = !$isBody;
    }
    // .. transpose `back in'
    $model = array();
    $n = mendTogether($bodyColumn, $punctuationColumn, '');
    for ($i = 0; $i < $n; $i++) {
        $model[$i] = array(
            'body' => $bodyColumn[$i],
            'punctuation' => $punctuationColumn[$i],
        );
    }
    return $model;
}

function mendTogether (&$array1, &$array2, $defaultValue)
{
    $n1 = mendTo($array1, $array2, $defaultValue);
    $n2 = mendTo($array2, $array1, $defaultValue);
    return max($n1, $n2);
}

function mendTo($etalon, &$target, $defaultValue)
{
    $n = count($etalon);
    for ($i = 0; $i < $n; $i++) {
        mendItem($target, $i, $defaultValue);
    }
    return $n;
}

function mendItem (&$array, $index, $defaultValue)
{
    mendVar($array[$index], $defaultValue);
}

function mendVar(&$variable, $defaultValue)
{
    $unset = !isset($variable);
    if ($unset) {
        $variable = $defaultValue;
    }
}

function emptiless($segments)
{
    $emptiless = array();

    foreach ($segments as $segment) {
        $body = $segment['body'];
        $punctuation = $segment['punctuation'];
        $superfluous = empty($body) && empty($punctuation);
        if (!$superfluous) {
            $emptiless[] = $segment;
        }
    }

    return $emptiless;
}

function extension($fullName)
{
    return pathinfo($fullName, PATHINFO_EXTENSION);
}

function withoutExtension($fullName)
{
    $dirname  = pathinfo($fullName, PATHINFO_DIRNAME);
    $filename = pathinfo($fullName, PATHINFO_FILENAME);
    return "$dirname/$filename";
}


// Testing

function asPrint($thing)
{
    if (is_array($thing)) {
        var_dump($thing);
    } else {
        echo $thing;
    }
}

function asCell($thing)
{
    echo "<td>";
    asPrint($thing);
    echo "</td>";
}

function asGroupedTables($groupedResultset, $caption)
{
    foreach ($groupedResultset as $groupattr => $resultset) {
        asHtmlTable($resultset, "$caption = $groupattr:");
    }
}

function asHtmlTable ($resultSet, $caption = '', $breakline = true)
{
?>
<table border="1">
<?php
    if ($caption) {
        echo "<caption>$caption</caption>";
    }
    foreach ($resultSet as $i => $row) {
?>
    <tr>
<?php
        if ($i == 0) {
            foreach ($row as $attr => $cell) {
                echo "<th>$attr</th>";
            }
            echo '</tr><tr>';
            foreach ($row as $cell) {
                asCell($cell);
            }
        } else {
            foreach ($row as $cell) {
                asCell($cell);
            }
        }
?>
    </tr>
<?php
    }
?>
</table>
<?php
    if ($breakline) {
        echo "<br/>";
    }
}


function indexVal($a)
{
    return $a == '' ? -1 : intval($a);
}

function intvals($numStrings)
{
    return array_map('intval', $numStrings);
}


/*
mendVar ($xxx, "mendVar OK\n");
echo $xxx;
$arr = array();
mendItem($arr, 0, "mendItem OK\n");
echo $arr[0];

$array1 = array();
$array2 = array(1, 2, 3);
mendTo($array1, $array2, 0);
var_dump($array1);
var_dump($array2);

mendTo($array2, $array1, 0);
var_dump($array1);
var_dump($array2);

$array1 = array();
$array2 = array(1, 2, 3);
mendTogether($array1, $array2, 0);
var_dump($array1);
var_dump($array2);

$words = splitToWords("Little Red Hood, a tale from Mr. Grimm.");
var_dump($words);
$sentences = splitToSentences('__Egy mondat.+Ket mondat.++Harom mondat.+++');
var_dump($sentences);

importStory('Once upon a time there was a dear little girl who was loved by every one who looked at her, but most of all by her grandmother, and there was nothing that she would not have given to the child. Once she gave her a little cap of red velvet, which suited her so well that she would never wear anything else; so she was always called \'Little Red-Cap.\'', 0);
*/
?>
