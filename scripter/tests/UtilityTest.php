<?php

require dirname(__FILE__) . '/../public/utility.php';

class UtilityTest extends PHPUnit_Framework_TestCase
{
    public function testSentenceToBodyAndPunctuation()
    {
        $arr = sentenceToBodyAndPunctuation("A sentence.");
        $this->assertEquals(array('A sentence', '.'), $arr);

        $arr = sentenceToBodyAndPunctuation("This looks? like 3(!) sentences but it is one...");
        $this->assertEquals(array('This looks? like 3(!) sentences but it is one', '...'), $arr);

        $arr = sentenceToBodyAndPunctuation("A sentence without ending");
        $this->assertEquals(array('A sentence without ending', ''), $arr);
    }
}
