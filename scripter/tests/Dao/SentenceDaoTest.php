<?php

require_once dirname(__FILE__) . '/../DbTestCase.php';
require_once dirname(__FILE__) . '/../../public/application/Dao/SentenceDao.php';
require_once dirname(__FILE__) . '/../../public/application/Domain/Sentence.php';

class SentenceDaoTest extends DbTestCase
{
    public function getDataSet()
    {
        $path = dirname(__FILE__);
        return $this->createXMLDataSet("$path/sentence.xml");
    }

    public function testGetById()
    {
        $sentenceDao = new Dao_SentenceDao();

        $sentence0 = $sentenceDao->getById(0);
        $sentence1 = $sentenceDao->getById(1);
        $sentence2 = $sentenceDao->getById(2);

        $this->assertFalse($sentence0);

        $this->assertEquals(1, $sentence1->id);
        $this->assertEquals(1, $sentence1->scriptId);
        $this->assertEquals("Egyszer egy időben havazott", $sentence1->meaningTxt);
        $this->assertEquals(1, $sentence1->orderNo);
        $this->assertNull($sentence1->sourceId);

        $this->assertFalse($sentence2);

        // Check whether state of database remained intact

        $path = dirname(__FILE__);
        $expectedDataSet = $this->createXMLDataSet("$path/sentence-getById.xml");

        $tableNames = array('sentence');
        $actualDataSet = $this->getConnection()->createDataSet($tableNames);

        $this->assertDataSetsEqual($expectedDataSet, $actualDataSet);
    }
}
?>
