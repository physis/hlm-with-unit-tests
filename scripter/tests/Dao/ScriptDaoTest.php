<?php

require_once dirname(__FILE__) . '/../../public/application/Dao/ScriptDao.php';
require_once dirname(__FILE__) . '/../../public/application/Domain/Script.php';
require_once dirname(__FILE__) . '/../DbTestCase.php';

class ScriptDaoTest extends DBTestCase
{
	// `getConnection' implementation has been already inherited

	public function getDataSet()
	{
		$path = dirname(__FILE__);
		return self::createXMLDataSet("$path/script.xml");
	}

	public function testGetById()
	{
		$scriptDao = new Dao_ScriptDao();
		$script = $scriptDao->getById('1');
		self::assertEquals(1, $script->id);
		self::assertEquals('Snow White', $script->title);
		self::assertEquals(0, $script->isDeleted);
		self::assertEquals('I', $script->statusId);
		self::assertNull($script->sourceId);
	}
}
?>
