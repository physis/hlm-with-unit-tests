<?php

require_once dirname(__FILE__) . '/../../public/application/Dao/SWordDao.php';

class SWordDaoTest extends DbTestCase
{
    public function getDataSet()
    {
        $path = dirname(__FILE__);
        return $this->createXMLDataSet("$path/sword.xml");
    }

    public function testFindBySentenceId()
    {
        $sWordDao = new Dao_SWordDao();
        $sWords = $sWordDao->findBySentenceId(1);
        $this->assertEmpty($sWords);

        $path = dirname(__FILE__);
        $expectedDataSet = $this->createXMLDataSet("$path/sword-findBySentenceId.xml");

        $tableNames = array('sword');
        $actualDataSet = $this->getConnection()->createDataSet($tableNames);

        $this->assertDataSetsEqual($expectedDataSet, $actualDataSet);
    }
}
?>
