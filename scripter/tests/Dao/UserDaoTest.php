<?php

require_once dirname(__FILE__) . '/../../public/application/Dao/UserDao.php';
require_once dirname(__FILE__) . '/../../public/application/Domain/User.php';
require_once dirname(__FILE__) . '/../DbTestCase.php';

class UserDaoTest extends DbTestCase
{
    // `getConnection' implementation is already inherited

    public function getDataSet()
    {
        $path = dirname(__FILE__);
        return self::createXMLDataSet("$path/user.xml");
    }

    public function testFindUserByName()
    {
        $userDao = new Dao_UserDao();

        // Check whether data are retrieved:

        $user = $userDao->findUserByName('Joe');
        self::assertEquals(1, $user->id);
        self::assertEquals( 'Joe', $user->name);
        self::assertEquals('qwerty', $user->password);

        $none = $userDao->findUserByName('Jack');
        self::assertFalse($none);

        // Check whether table has remained intact:

        $tables = array('user');
        $actualDataSet = self::getConnection()->createDataSet($tables);

        $path = dirname(__FILE__);
        $expectedDataSet = self::createXMLDataSet("$path/user-findUserByName.xml");

        self::assertDataSetsEqual($expectedDataSet, $actualDataSet);

    }
}
?>
