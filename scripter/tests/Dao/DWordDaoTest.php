<?php

require_once dirname(__FILE__) . '/../../public/application/Dao/DWordDao.php';
require_once dirname(__FILE__) . '/../../public/application/Domain/DWord.php';
require_once dirname(__FILE__) . '/../../public/application/Domain/DWordMeaning.php';
require_once dirname(__FILE__) . '/../DbTestCase.php';

class DWordDaoTest extends DBTestCase
{
    public function getDataSet()
    {
        $path = dirname(__FILE__);
        return self::createXMLDataSet("$path/dword.xml");
    }

    public function testMapToDWord()
    {
        $map = array(
            'dword_id' => 1,
            'word' => 'snow',
        );
        $dWordFromMap = Dao_DWordDao::mapToDWord($map);
        $dWordFromConstructor = new Domain_DWord(1, 'snow');
        self::assertEquals($dWordFromConstructor, $dWordFromMap);
        self::assertEquals(1, $dWordFromMap->id);
        self::assertEquals('snow', $dWordFromMap->word);
    }

    public function testMapToDWordMeaning()
    {
        $map = array(
            'dword_meaning_id' => 1,
            'dword_id' => 1,
            'meaning_txt' => 'hó',
        );
        $dWordMeaningFromMap = Dao_DWordDao::mapToDWordMeaning($map);
        $dWordMeaningFromConstructor = new Domain_DWordMeaning(1, 1, 'hó');
        self::assertEquals($dWordMeaningFromConstructor, $dWordMeaningFromMap);
        self::assertEquals(1, $dWordMeaningFromMap->id);
        self::assertEquals(1, $dWordMeaningFromMap->dwordId);
        self::assertEquals('hó', $dWordMeaningFromMap->meaningTxt);
    }

    public function testGetIdOrNullByWord()
    {
        $dWordDao = new Dao_DWordDao();
        $id = $dWordDao->getIdOrNullByWord("snow");
        $this->assertEquals(1, $id);
        $id = $dWordDao->getIdOrNullByWord("gibberish");
        $this->assertNull($id);

        $path = dirname(__FILE__);
        $expectedDataSet = $this->createXMLDataSet("$path/dword-getIdOrNullByWord.xml");
        $actualDataSet = $this->getConnection()->createDataSet(array('dword', 'dword_meaning'));
        $this->assertDataSetsEqual($expectedDataSet, $actualDataSet);
    }
}
?>
