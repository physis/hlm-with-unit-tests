<?php
class Db
{
    protected static $db = NULL;

    protected static function createDb()
    {
        $pdo = new PDO("sqlite::memory:");

        $nUser = $pdo->exec("CREATE TABLE user(user_id INTEGER PRIMARY KEY, user_name VARCHAR(32) NOT NULL, password VARCHAR(32) NOT NULL)");
        $nDword = $pdo->exec("CREATE TABLE dword(dword_id INTEGER PRIMARY KEY, word VARCHAR(40) NOT NULL)");
        $nDwordMeaning = $pdo->exec("CREATE TABLE dword_meaning(dword_meaning_id INTEGER PRIMARY KEY, dword_id INTEGER NOT NULL, meaning_txt VARCHAR(250), FOREIGN KEY (dword_id) REFERENCES dword(dword_id))");
        $nScript = $pdo->exec("CREATE TABLE script(script_id INTEGER PRIMARY KEY, title VARCHAR(255) NOT NULL, is_deleted BOOLEAN NOT NULL DEFAULT 0, status_id VARCHAR(1) NOT NULL DEFAULT 'I', source_script_id INTEGER, FOREIGN KEY (source_script_id) REFERENCES script(script_id))");
        $nSentence = $pdo->exec("CREATE TABLE sentence(sentence_id INTEGER PRIMARY KEY, script_id INTEGER NOT NULL, meaning_txt VARCHAR(10000), order_no INTEGER NOT NULL, source_sentence_id INTEGER, FOREIGN KEY (script_id) REFERENCES script(script_id), FOREIGN KEY (source_sentence_id) REFERENCES sentence(sentence_id))");
        $nSwordMeaning = $pdo->exec("CREATE TABLE sword (sword_id INTEGER PRIMARY KEY, sentence_id INTEGER NOT NULL, word VARCHAR(40) NOT NULL, word_suffix VARCHAR(40), dword_id INTEGER, dword_meaning_id INTEGER, grammar_form VARCHAR(40), origin_txt VARCHAR(250), stylistics_txt VARCHAR(250), spelling_txt VARCHAR(250), context_txt VARCHAR(250), order_no INTEGER NOT NULL, source_sword_id INTEGER, FOREIGN KEY (sentence_id) REFERENCES sentence(sentence_id), FOREIGN KEY (dword_id) REFERENCES dword(dword_id), FOREIGN KEY (dword_meaning_id) REFERENCES dword_meaning (dword_meaning_id), FOREIGN KEY (source_sword_id) REFERENCES sword(sword_id))");

        if ($nUser === FALSE || $nDword === FALSE || $nDwordMeaning === FALSE || $nScript === FALSE || $nSentence === FALSE || $nSwordMeaning === FALSE) {
            throw new Exception('Problem with setting db-schema in-memory');
        } else {
            return $pdo;
        }
    }

    public static function getDb()
    {
        if (self::$db == NULL) {
            self::$db = self::createDb();
        }
        return self::$db;
    }
}
?>
