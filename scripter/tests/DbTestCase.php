<?php
require_once dirname(__FILE__) . '/Db.php';

abstract class DbTestCase extends PHPUnit_Extensions_Database_TestCase
{
    protected static $connection = NULL;

    protected static function createConnection()
    {
        $path = dirname(__FILE__);
        $pdo = Db::getDb();
        return self::createDefaultDBConnection($pdo, ":memory:");
    }

    public final function getConnection()
    {
        if (self::$connection == NULL)
        {
            self::$connection = self::createConnection();
        }
        return self::$connection;
    }
}
?>
