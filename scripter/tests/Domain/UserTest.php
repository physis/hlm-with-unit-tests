<?php

require_once dirname(__FILE__) . '/../../public/application/Domain/User.php';

class UserTest extends PHPUnit_Framework_TestCase
{
    // Fixture:

    protected $user;

    protected function setUp()
    {
        $this->user = new Domain_User();

        $this->user->id = 1;
        $this->user->name = 'Joe';
        $this->user->password = 'qwerty';
    }

    protected function tearDown()
    {
        $this->user = NULL;
    }

    // Tests:

    public function testCheckPassword()
    {
        $qwertz = $this->user->checkPassword('qwertz');
        $qwerty = $this->user->checkPassword('qwerty');
        self::assertFalse($qwertz);
        self::assertTrue($qwerty);

        // As it is a unit test,
        // we must test also without constructor call:

        $this->user->password = 'QWERTY';

        $QWERTZ = $this->user->checkPassword('QWERTZ');
        $QWERTY = $this->user->checkPassword('QWERTY');
        self::assertFalse($QWERTZ);
        self::assertTrue($QWERTY);
    }
}
?>
