<?php

class Mvc
{

    private $interceptorPreArray = array();
    
    public function __construct(&$views)
    {
        $this->views = $views;
    }
    
    private function interceptPre()
    {
        foreach ($this->interceptorPreArray as $interceptorPre) {
            if (!$interceptorPre()) return FALSE;
        }
        return TRUE;
    }
    
    public function addInterceptorPre($fun)
    {
        $this->interceptorPreArray[] = $fun;
    }

    private function createController()
    {
        // e.g. from xxx.php/foo -> Controller_Foo
        $pathParts = explode('/', $_SERVER['PATH_INFO']);
        $controllerName = ucfirst($pathParts[1]);
        $fullControllerName = "Controller_$controllerName";
        return new $fullControllerName;
    }

    private function redirect($url) {
        if (DEV_MODE) {
            echo "redirect to <a href='$url'>$url</a>";
        } else {
            header("Location: $url");
        }
    }

    private function view($view, $model)
    {
        if (!isset($view)) return; // no view is fine; it means we are done
        if (substr($view, 0, 9) == 'redirect:') {
            $url = substr($view, 9);
            $this->redirect($url);
            return;
        }
        extract($model);
        $templateContent = VIEW_DIR . "/$view.php";
        $viewConf = isset($this->views[$view]) ? $this->views[$view] : NULL;
        if (isset($viewConf) && isset($viewConf['head'])) {
            $viewConfHead = $viewConf['head'];
            $templateHead = VIEW_DIR . "/$viewConfHead.php";
        }
        require VIEW_DIR . "/template.php";
    }

    public function dispatch()
    {
        try {
            if (!$this->interceptPre()) return;
            $controller = $this->createController();
            list($view, $model) = $controller->action();
        } catch (Exception $ex) {
            $this->view('error', array(
                    'templateTitle' => V::_('Error'),
                    'exception' => $ex));
            return;
        }
        $this->view($view, $model);
    }

}

